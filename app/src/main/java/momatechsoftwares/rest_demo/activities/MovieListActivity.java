package momatechsoftwares.rest_demo.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import momatechsoftwares.rest_demo.R;

public class MovieListActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_list);
    }
}
